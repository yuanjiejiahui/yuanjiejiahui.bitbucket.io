// 当前时间
function getDate(){
    var date = new Date();
    var year = date.getFullYear();
    var month = date.getMonth() + 1;
    month = month < 10 ? '0' +month : month;
    var arr = ['星期日','星期一','星期二','星期三','星期四','星期五','星期六'];
    var day = date.getDay() + 1;
    var dates =date.getDate();
    var hours = date.getHours();
    hours = hours < 10 ? '0' +hours : hours;
    var minutes = date.getMinutes();
    minutes = minutes < 10 ? '0' +minutes : minutes;
    var seconds = date.getSeconds();
    seconds = seconds < 10 ? '0'+seconds : seconds;
    dateEle.innerHTML = '当前时间: ' + year + '-' + month + '-' + dates + ' ' + arr[day] + ' ' +hours + ':' + minutes + ':' +seconds;
    return 0;
}
var dateEle = document.querySelector('#date');
setInterval(getDate,1000);


//时间倒计时
function countDown(){
    var time ='2021-7-1 00:00:00'
    var nowTime  = +new Date();
    var inputTime  = +new Date(time);
    var times = (inputTime - nowTime) / 1000 ;
    var day = parseInt(times/3600/24);//天
    day = day < 10 ? '0' +day : day;
    var hours = parseInt(times/3600%24);//时
    hours = hours < 10 ? '0' +hours : hours;
    var minutes = parseInt(times/60 % 60);//分
    minutes = minutes < 10 ? '0' +minutes : minutes;
    var seconds = parseInt(times % 60);
    seconds = seconds < 10 ? '0' +seconds : seconds;
    dateElex.innerHTML = '距离100周年建党庆典还剩:' + day + '天' + hours + '时' + minutes + '分' + seconds + '秒';
    return 0;
}
var dateElex = document.querySelector('#dateDown');
setInterval(countDown,1000);


//分时问候
var img = document.querySelector('#hello');
function Hello(){
    var date = new Date();
    var h = date.getHours();
    if(h<12){
        img.src='image/moringHello.jpg';
    }else if(h<18){
        img.src='image/afternoonHello.png';
    }else{
        img.src='image/nightHello.png';
    }
}
Hello();